using system;

class Program
{
    static void Main(string[] args)
    {
        string? cin, firstName, lastName, tel;
        float? somme;

        # Create Compte Client 1
        
        Console.Write($"Compte 1:\nDonner Le CIN: ");
        cin = Console.ReadLine();
        Console.Write($"Donner Le Nom: ");
        firstName = Console.ReadLine();
        Console.Write($"Donner Le Prénom: ");
        lastName = Console.ReadLine();
        Console.Write($"Donner Le numéro de télephone: ");
        tel = Console.ReadLine();

        Compte cpt1 = new Compte(new Client(cin, firstName, lastName, tel));
        Console.WriteLine($"Détails du compte:{cpt1.Resumer()}");
        
      
        # Crediter & Debiter Compte Client 1
        
        //Crediter cpt 1
        Console.Write($"Donner le montant à déposer: ");
        somme = float.Parse(Console.ReadLine());
        cpt1.Crediter((float)somme);
        Console.WriteLine($"Opération bien effectuée{cpt1.Resumer()}");
        //Debiter cpt 1
        Console.Write($"Donner le montant à retirer: ");
        somme = float.Parse(Console.ReadLine());
        cpt1.Debiter((float)somme);
        Console.WriteLine($"Opération bien effectuée{cpt1.Resumer()}");
        
     
        # Create Compte Client 2

        Console.Write($"\n\n\nCompte 2:\nDonner Le CIN: ");
        cin = Console.ReadLine();
        Console.Write($"Donner Le Nom: ");
        firstName = Console.ReadLine();
        Console.Write($"Donner Le Prénom: ");
        lastName = Console.ReadLine();
        Console.Write($"Donner Le numéro de télephone: ");
        tel = Console.ReadLine();

        Compte cpt2 = new Compte(new Client(cin, firstName, lastName, tel));
        Console.WriteLine($"Détails du compte:{cpt2.Resumer()}");

    
    

        //Crediter cpt2 -> cpt1
        Console.WriteLine($"Crediter le compte {cpt2.Code} à partir du compte {cpt1.Code}");
        Console.Write($"Donner le montant à déposer: ");
        somme = float.Parse(Console.ReadLine());
        cpt2.Crediter((float)somme, cpt1);
        Console.WriteLine($"Opération bien effectuée");
        //Debiter cpt1 -> cpt2
        Console.WriteLine($"Débiter le compte {cpt1.Code} et créditer le compte {cpt2.Code}");
        Console.Write($"Donner le montant à retirer: ");
        somme = float.Parse(Console.ReadLine());
        cpt1.Debiter((float)somme, cpt2);
        Console.Write($"Opération bien effectuée");

   

        Console.WriteLine($"{cpt1.Resumer()}{cpt2.Resumer()}");
        Console.Write($"\n\n\n{Compte.NombreCompteCree()}");
        Console.ReadKey();
    }
}



class Client
{
    public string CIN {get; set;}
    public string Nom {get; set;}
    public string Prenom {get; set;}
    public string Tel {get; set;}

    public string Afficher()
    {
        return $@"CIN: {CIN}
        NOM: {Nom}
        Prénom: {Prenom}
        Tél : {Tel}";
    }

    public Client(string cin, string nom, string prenom, string tel)
    {
        CIN = cin.ToUpper();
        Nom = nom;
        Prenom = prenom;
        Tel = tel;   
    }

    public Client(string cin, string nom, string prenom)
    {
        CIN = cin.ToUpper();
        Nom = nom;
        Prenom = prenom;
        Tel = "";   
    }
}

class Compte
{
    private float solde;
    public float Solde {get{return solde;}}
    private uint code;
    public uint Code {get{return code;}}
    public Client Proprietaire {get; set;}

    private static uint compteur = 0;

    public string Resumer()
    {
        return $@"
        ************************
        Numéro de Compte: {Code}
        Solde de compte: {Solde}
        Propriétaire du compte:
        {Proprietaire.Afficher()}
        ************************";
    }

    public static string NombreCompteCree()
    {
        return $"Le nombre de comptes crées: {compteur}";
    }

    public void Debiter(float montant, Compte compteClient)
    {
        Debiter(montant);
        compteClient.Crediter(montant);
    }

    public void Debiter(float montant)
    {
        solde -= montant;
    }

    public void Crediter(float montant, Compte compteClient)
    {
        Crediter(montant);
        compteClient.Debiter(montant);
    }

    public void Crediter(float montant)
    {
        solde += montant;
    }

    public Compte(Client client, float montant)
    {
        Proprietaire = client;
        solde = montant;
        code = ++compteur;
    }

    public Compte(Client client)
    {
        Proprietaire = client;
        solde = default;
        code = ++compteur;
    }
}
